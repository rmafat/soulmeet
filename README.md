# SoulMeet

[[_TOC_]]

## Documentation
* [Cahier des charges](./docs/cahier-des-charges.md)
* [Schéma de base de données](./docs/database-schema.png)

## Installation
### Pré-requis

Vous devez avoir d'installé sur voter machine :
- [Docker](https://www.docker.com/get-started)
- [Docker composer](https://docs.docker.com/compose/install/) 

### Mise en place de l'environnement
Récupération et installation :
```bash
git clone https://gitlab.com/rmafat/soulmeet.git
cd soulmeet
docker-compose up

docker exec -it soulmeet_symfony bash
> composer install
```

Modifier le fichier /etc/hosts de votre machine locale :
```bash
127.0.0.1 soulmeet.local.fr
```

Sur votre navigateur favoris, allez sur l'adresse : `soulmeet.local.fr`.  
Pour accéder au **PhpMyAdmin**, allez sur l'adresse : `soulmeet.local.fr:8080`.

### Data
Mettre à jour la base de donnée : 
```bash
php bin/console doctrine:schema:update --force
```

Pour load les **fixtures**, dans le container, entrer la commande suivante:

```bash
php -d memory_limit=-1 bin/console doctrine:fixtures:load
```

### Styles

Dans le container _soulmeet_symfony_, lancer les commandes suivantes :

```bash
yarn install
yarn encore production
```

Durant les développements :
```bash
yarn encore dev --watch
``` 
