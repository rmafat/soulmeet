# SoulMeet - Cahier des charges

[[_TOC_]]

## Concept

SoulMeet est une **plateforme de rencontres**. Elle se base sur des critères de recherche physiques ainsi que sur les centres d’intérêt. 

En arrivant sur le site, l’utilisateur crée un compte. Lors de cette création il va renseigner ses _informations personnelles_ habituelles (identité, âge, email, etc.) mais aussi une description physique et pourra également choisir, parmi une liste prédéfinie de centres d’intérêt, lesquels lui correspondent ou pas (à l’aide d’un sélecteur positif/négatif).  
Il est ensuite en mesure de définir des _critères de recherche_ basés sur le physique, ou les centres d’intérêt qu’il aura précédemment sélectionnés.

Le site comporte 4 pages : 
* Une page d’accueil, accessible publiquement et présentant le site et son fonctionnement
* Une page de gestion de compte et des préférences de recherche
* Une page listant les potentielles rencontres qui auront été définies par notre algorithme
* Une page de chat sur laquelle se trouveront les demandes en attente et les conversations engagées

Quand un utilisateur souhaite _entrer en contact_ avec une potentielle rencontre, il a la possibilité de lui envoyer un premier message. Le destinataire pourra alors refuser son message, ce qui mettra fin à la relation, ou l’accepter, ce qui ouvrira la possibilité aux deux personnes de discuter ensemble.

Sur chaque profil sera affiché un _pourcentage de compatibilité_ entre l’utilisateur connecté et la potentielle rencontre, et les informations de ce profil.



## Technologies

Le site est un monolithe basé sur **Symfony**, avec une base de données **MySQL**.

Le tout est mis en place dans un environnement **docker**, géré avec docker-compose.

Le projet est hébergé sur Gitlab.

De l'**intégration continue** est mise en place directement sur Gitlab, pour lancer des tests. 



 ## Pages

Pour accéder aux fonctionnalités du site, il faut être authentifié.



### Menu

Le menu reprend les informations suivantes :

**Menu deconnecté** :

- Logo
- Connexion
- Inscription

**Menu connecté**

- Logo
- Rencontrer
- Chat
- Une image de profil qui déplie un menu pour accéder à 
  - Mon profil
  - Déconnexion



### Page d'accueil

La page d'accueil présente le fonctionnement du site. Elle est *publiquement* accessible.



### Page de connexion 

La connexion se fait par email / mot de passe.

Si l'utilisateur n'est pas encore inscrit, un lien vers l'inscription est disponible. 



### Page d'inscription

L'inscription se déroule en *4 étapes* :

**L'identité de l'utilisateur** :

- Nom*
- Prénom*
- Email*
- Mot de passe* 
  - Minimum 8 caractères, dont une majuscule, une minuscule, un chiffre et un caractère spécial
- Date de naissance* 
  - Minimum 18 ans
- Sexe*
- Département*
  - France uniquement pour le moment
- Photos* 
  - Au moins une
- Description

**Une description physique de l'utilisateur** :

Pour chaque champ, une seule option est possible. 

- Couleur de cheveux
- Couleur de peau
- Couleur des yeux
- Taille
  - Input range
- Morphologie

**Centres d'intérêts de l'utilisateur** :

Chaque centre d'intérêt possède un curseur pouvant être *positif*, *négatif* ou *neutre*.

**Préférences de recherche** :

Pour chaque champ, il est possible de définir plusieurs critères.
Si rien n'est choisi pour un champ, on considère que tout est sélectionné.

- Genre

- Localisation (département / pays)
- Couleur de peau (Noir, métisse, arabe, asiatique, indien, blanc)
- Couleur des yeux
- Couleur de cheveux
- Taille
- Poids
- Tranche d'âge 

**Importance des centres d'intérêts** : 

Pour chaque centre d'intérêt non-neutre, il est possible de dire si c'est un **critère important** pour la recherche ou non.



### Page de profil 

La page de profil reprend les informations de l'inscription, mais seules les informatiosn suivantes sont **modifiables** : 

- Mot de passe

- Département

- Photos

- Description

- Couleur de cheveux

- Taille

- Morphologie

- Centres d'intérêts

- Tous les critères de recherche

  

### Page de rencontre

La page de rencontre liste toutes les personnes qui correspondent aux critères de recherche.

On retrouve une grille de **cartes**. Chaque carte correspond à une personne. Sur cette carte, on retrouve les informations suivantes : 

- Photo de profil
- Prénom
- Age
- Description
- Un pourcentage de correspondance
- Un bouton ''X" pour fermer la carte => Cliquer sur ce bouton signifie que cette personne ne pourra plus nous être proposée.
- Un bouton pour envoyer un message => Il n'est possible d'envoyer qu'un seul message (dans un premier temps).



Au survol de la carte, le pourcentage a un effet de léger battement, pour appeler l'utilisateur au clic.
Lorsque l'on clique sur la bulle de pourcentage, cela ouvre une **modale** avec tous les centres d'intérêts non nulsle profil complet de la personne concernée.



### Page de chat

*[BONUS]*

La partie gauche de la page liste toutes les conversations de l'utilisateur.
Chacune de ces conversations reprend :

- La photo de la relation
  - Un petit badge apparait dans le coin de la photo si un message est non lu
- Son prénom
- Le début du dernier message
  - Celui ci est en gras si le message est non lu



Lorsque quelqu'un envoie un message, l'autre personne peut voir ce message, et doit aller *Accepter* ou *Refuser* cette demande de contact.

- Si on *accepte*, la conversation peut commencer
- Si on *refuse*, la conversation est effacée pour la personne ayant refusée, et un message automatique de refus est envoyé à l'envoyeur.



En ouvrant une conversation, il est possible de **bloquer** l'utilisateur. Cette action ouvre une **popin** pour confimer cette action irréversible.


## Algorithme

L'algorithme de rencontre est basé sur 2 types de préférences :

- Les hards préférences : Les utilisateurs doivent obligatoirement matcher avec ces préférences
- Les softs préférences : Permettent de définir un pourcentage basé sur les centres d'intérêts en commun



Les <u>hards préférences</u> sont définies par les critères suivants : 

- Genre

- Localisation (département / pays)
- Couleur de peau (Noir, métisse, arabe, asiatique, indien, blanc)
- Couleur des yeux
- Couleur de cheveux
- Morphologie
- Tranche d'âge 



Les softs préférences sont définies par les centres d'intérêts. 
Pour calculer le pourcentage, il faut appliquer le calcul suivant *sur chaque centre d'intérêt* défini comme important par la personne: 

```10 - abs(personal_value - relation_value) ```

Ensuite, on définit une moyenne de tous ces résultats, que l'on multiplie par 10 pour avoir un poucentage.

Ce calcul doit être fait pour chacune des deux personnes de la relation, afin de pouvoir faire un moyenne des 2 pourcentages. 



| Champs             | Type                      | Requirements                                                 |
| ------------------ | ------------------------- | ------------------------------------------------------------ |
| Genre              | Checkbox                  |                                                              |
| Localisation       | Select multiple           | Entity Department                                            |
| Tranche d'age      | Double select (min / max) | Le min doit être >= max. Les âges ne peuvent être < 18 et > 120. |
| Couleur de peau    | Entity multiple           | Entity Skin                                                  |
| Couleur de cheveux | Entity multiple           | Entity Hair                                                  |
| Couleur des yeux   | Entity multiple           | Entity Eyes                                                  |
| Morphologie        | Entity multiple           | Entity Shape                                                 |
| Centres d'intérêt  | Checkbox                  | Entity UserHobbies                                           |