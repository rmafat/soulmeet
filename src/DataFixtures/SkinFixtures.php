<?php

namespace App\DataFixtures;

use App\Entity\Skin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class SkinFixtures
 */
class SkinFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $skinColors = [
            'Pâle'        => '#FDFBF5',
            'Blanc clair' => '#F9DFB7',
            'Blanc'       => '#EFC98F',
            'Jaune'       => '#FFE8AC',
            'Bronzé'      => '#E5CC8C',
            'Mat clair'   => '#C9964A',
            'Mat'         => '#9D7830',
            'Mat foncé'   => '#4F3C0D',
            'Noir'        => '#000000',
        ];

        foreach ($skinColors as $name => $color) {
            $skin = new Skin();
            $skin->setName($name)
                ->setColor($color);

            $manager->persist($skin);
        }

        $manager->flush();
    }
}
