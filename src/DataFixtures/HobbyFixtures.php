<?php

namespace App\DataFixtures;

use App\Entity\Hobby;
use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class HobbyFixtures
 */
class HobbyFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $hobbies = [
            'Guitare'           => '/assets/icons/guitar.png',
            'Piano'             => '/assets/icons/piano.png',
            'Saxophone'         => '/assets/icons/saxophone.png',
            'Batterie'          => '/assets/icons/drums.png',
            'Foot'              => '/assets/icons/football.png',
            'Basket'            => '/assets/icons/basketball.png',
            'Musculation'       => '/assets/icons/exercise.png',
            'Course'            => '/assets/icons/running.png',
            'Vélo'              => '/assets/icons/bycicle.png',
            'Bière'             => '/assets/icons/beer.png',
            'Fast Food'         => '/assets/icons/burger.png',
            'Gastronomique'     => '/assets/icons/gastronomy.png',
            'Cinéma'            => '/assets/icons/clapperboard.png',
            'TV'                => '/assets/icons/tv.png',
            'Jeux vidéo'        => '/assets/icons/console.png',
            'Code'              => '/assets/icons/code.png',
            'Peinture / Dessin' => '/assets/icons/painting.png',
            'Fête'              => '/assets/icons/party.png',
            'Shopping'          => '/assets/icons/shopping.png',
            'Chant'             => '/assets/icons/sing.png',
            'Rugby'             => '/assets/icons/rugby.png',
            'Yoga'              => '/assets/icons/yoga.png',
            'Danse'             => '/assets/icons/dancer.png',
            'Lecture'           => '/assets/icons/book.png',
            'Photo'             => '/assets/icons/camera.png',
        ];

        foreach ($hobbies as $name => $url) {
            $iconUrlExploded = explode('/', $url);
            $iconName        = $iconUrlExploded[3];

            $icon = new Picture();
            $icon->setName($iconName)
                ->setUrl($url)
                ->setAlternativeText($name.' icone');

            $hobby = new Hobby();
            $hobby->setName($name)
                ->setIcon($icon);

            $manager->persist($icon);
            $manager->persist($hobby);
        }

        $manager->flush();
    }
}
