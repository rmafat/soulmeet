<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use App\Entity\Shape;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class ShapeFixtures
 */
class ShapeFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $menShapes = [
            'Fin'      => '/assets/icons/man-thin.png',
            'Normal'   => '/assets/icons/man-normal.png',
            'Musclé'   => '/assets/icons/man-strong.png',
            'Enrobé'   => '/assets/icons/man-coated.png',
            'Surpoids' => '/assets/icons/man-overweight.png',
        ];
        $womenShapes = [
            'Fine'     => '/assets/icons/man-thin.png',
            'Normale'  => '/assets/icons/man-normal.png',
            'Musclée'  => '/assets/icons/man-strong.png',
            'Enrobée'  => '/assets/icons/man-coated.png',
            'Surpoids' => '/assets/icons/man-overweight.png',
        ];

        $this->createIconAndShapeFromArray($manager, $menShapes);
        $this->createIconAndShapeFromArray($manager, $womenShapes);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param array         $shapes
     */
    private function createIconAndShapeFromArray($manager, $shapes)
    {
        foreach ($shapes as $shapeName => $iconUrl) {
            $iconUrlExploded = explode('/', $iconUrl);
            $iconName        = $iconUrlExploded[3];
            // The following line will transform, for exemple, "man-thin.png" into "man thin icon"
            $iconAlternativeText = preg_replace('/(-|\.png)/', ' ', $iconName).'icon';

            $icon = new Picture();
            $icon->setUrl($iconUrl)
                ->setName($iconName)
                ->setAlternativeText($iconAlternativeText);

            $shape = new Shape();
            $shape->setName($shapeName)
                ->setIcon($icon);

            $manager->persist($icon);
            $manager->persist($shape);
        }
    }
}
