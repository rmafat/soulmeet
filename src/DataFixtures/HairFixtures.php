<?php

namespace App\DataFixtures;

use App\Entity\Hair;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class HairFixtures
 */
class HairFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $hairColors = [
            'Noir'        => '#000000',
            'Gris'        => '#BBBBBB',
            'Blanc'       => '#F8F8F8',
            'Brun'        => '#715122',
            'Châtain'     => '#B48848',
            'Blond'       => '#EAD938',
            'Roux'        => '#DD8247',
            'Chauve'      => null,
            'Multicolore' => 'linear-gradient(90deg, #f00000, #f00000 16.67%, #ff8000 16.67%, #ff8000 33.33%, #ffff00 33.33%, #ffff00 50%, #007940 50%, #007940 66.67%, #4040ff 66.67%, #4040ff 83.33%, #a000c0 83.33%, #a000c0)',
        ];

        foreach ($hairColors as $name => $color) {
            $hair = new Hair();
            $hair->setName($name)
                ->setColor($color);

            $manager->persist($hair);
        }

        $manager->flush();
    }
}
