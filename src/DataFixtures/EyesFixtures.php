<?php

namespace App\DataFixtures;

use App\Entity\Eyes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EyesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $eyesColors = [
            'Noir'   => '#000000',
            'Marron' => '#704E0E',
            'Bleu'   => '#35A3E1',
            'Vert'   => '#1AB746',
        ];

        foreach ($eyesColors as $name => $color) {
            $eyes = new Eyes();
            $eyes->setName($name)
                ->setColor($color);

            $manager->persist($eyes);
        }

        $manager->flush();
    }
}
