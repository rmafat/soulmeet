<?php

namespace App\DataFixtures;

use App\Entity\Department;
use App\Entity\Eyes;
use App\Entity\Hair;
use App\Entity\Hobby;
use App\Entity\Picture;
use App\Entity\SearchPreference;
use App\Entity\Shape;
use App\Entity\Skin;
use App\Entity\User;
use App\Entity\UserHobby;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class UserFixtures
 */
class UserFixtures extends Fixture
{
    /** @var HttpClientInterface */
    private $client;
    /** @var UserPasswordEncoderInterface */
    private $encoder;
    /** @var KernelInterface */
    private $kernel;

    /**
     * UserFixtures constructor.
     *
     * @param HttpClientInterface          $client
     * @param UserPasswordEncoderInterface $encoder
     * @param KernelInterface              $kernel
     */
    public function __construct(HttpClientInterface $client, UserPasswordEncoderInterface $encoder, KernelInterface $kernel)
    {
        $this->client  = $client;
        $this->encoder = $encoder;
        $this->kernel  = $kernel;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://randomuser.me/api/?results=1000&inc=gender,name,email,picture&format=json&nat=FR'
            );
            $results     = $response->toArray();
            $departments = $manager->getRepository(Department::class)->findAll();
            $shapes      = $manager->getRepository(Shape::class)->findAll();
            $hairs       = $manager->getRepository(Hair::class)->findAll();
            $eyes        = $manager->getRepository(Eyes::class)->findAll();
            $skins       = $manager->getRepository(Skin::class)->findAll();
            $hobbies     = $manager->getRepository(Hobby::class)->findAll();

            // We are going to generate 1000 fake users
            foreach ($results['results'] as $key => $result) {
                // Generated random data from database for physical attributes
                $randomDepartmentKey = array_rand($departments);
                $department          = $departments[$randomDepartmentKey];
                $randomShapeKey      = array_rand($shapes);
                $shape               = $shapes[$randomShapeKey];
                $randomHairKey       = array_rand($hairs);
                $hair                = $hairs[$randomHairKey];
                $randomEyesKey       = array_rand($eyes);
                $eye                 = $eyes[$randomEyesKey];
                $randomSkinKey       = array_rand($skins);
                $skin                = $skins[$randomSkinKey];

                if (
                    array_key_exists('gender', $result) &&
                    array_key_exists('name', $result) &&
                    array_key_exists('email', $result) &&
                    array_key_exists('picture', $result)
                ) {
                    $userPictures = $this->getRandomUserPictures($results, $key, $result);

                    $user            = new User();
                    $userUniqueEmail = preg_replace('/\./', uniqid().'.', $result['email'], 1);
                    $user->setEmail($userUniqueEmail)
                        ->setFirstname($result['name']['first'])
                        ->setLastname($result['name']['last'])
                        ->setGender(User::GENDERS[$result['gender']])
                        ->addPicture($userPictures[0])
                        ->addPicture($userPictures[1])
                        ->setPassword($this->generateRandomPassword($user))
                        ->setBirthdate($this->generateRandomBirthDate())
                        ->setDepartment($department)
                        ->setDescription(
                            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.'
                        )
                        ->setSize($this->generateRandomSize())
                        ->setShape($shape)
                        ->setHair($hair)
                        ->setEyes($eye)
                        ->setSkin($skin);

                    $searchPreferences = $this->getRandomUserSearchPreferences($user);
                    $user->setSearchPreferences($searchPreferences);

                    $userHobbies = $this->getRandomUserHobbies($hobbies, $user);
                    foreach ($userHobbies as $hobby) {
                        $user->addHobby($hobby);
                        $manager->persist($hobby);
                    }

                    $manager->persist($userPictures[0]);
                    $manager->persist($userPictures[1]);
                    $manager->persist($searchPreferences);
                    $manager->persist($user);
                }
            }

            $manager->flush();
        } catch (TransportExceptionInterface | ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface | Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $url
     *
     * @return string
     */
    private function downloadAndSaveImageFromUrl($url)
    {
        $content = file_get_contents($url);
        $path    = $this->kernel->getProjectDir().'/public/uploads/'.uniqid().'.jpg';
        $fp      = fopen($path, 'x');
        fwrite($fp, $content);
        fclose($fp);

        return $path;
    }

    /**
     * @param $results
     * @param $key
     * @param $result
     *
     * @return Picture[]
     */
    private function getRandomUserPictures($results, $key, $result)
    {
        // We're adding two pictures for each user the one generated and the one of the user after him
        $url      = $this->downloadAndSaveImageFromUrl($result['picture']['large']);
        $picture1 = new Picture();
        $picture1->setUrl($url)
            ->setName(explode('/', $url)[3])
            ->setAlternativeText($result['name']['last'].' '.$result['name']['first'].' profile picture')
            ->setPriority(1);
        if (array_key_exists(($key + 1), $results)) {
            $url = $this->downloadAndSaveImageFromUrl($results[($key + 1)]['picture']['large']);
        } elseif (array_key_exists(($key - 1), $results)) {
            // No user after the last one, so take the one before...
            $url = $this->downloadAndSaveImageFromUrl($results[($key - 1)]['picture']['large']);
        }
        $picture2 = new Picture();
        $picture2->setUrl($url)
            ->setName(str_replace('.jpg', '', explode('/', $url)[3]))
            ->setAlternativeText($result['name']['last'].' '.$result['name']['first'].' profile picture')
            ->setPriority(1);

        return [$picture1, $picture2];
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function generateRandomPassword($user)
    {
        $characters       = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!?&,;:/';
        $pass             = [];
        $charactersLength = strlen($characters) - 1;
        for ($i = 0; $i < 10; ++$i) {
            $n      = rand(0, $charactersLength);
            $pass[] = $characters[$n];
        }

        return $this->encoder->encodePassword($user, implode('', $pass));
    }

    /**
     * @return DateTime
     *
     * @throws Exception
     */
    private function generateRandomBirthDate()
    {
        try {
            $start  = 2055681; // 1970-01-24 11:01:21
            $end    = 992055681; // 2001-06-08 20:01:21
            $random = mt_rand($start, $end);

            return new DateTime(date('Y-m-d H:i:s', $random));
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function generateRandomSize()
    {
        $sizeMin = 150;
        $sizeMax = 220;

        return mt_rand($sizeMin, $sizeMax);
    }

    /**
     * @param User $user
     *
     * @return SearchPreference
     */
    private function getRandomUserSearchPreferences($user)
    {
        $ageMin       = 18;
        $ageMax       = 60;
        $randomAgeMin = mt_rand($ageMin, $ageMax);

        $searchPreferences = new SearchPreference();
        $searchPreferences->setGender(!$user->getGender())
            ->setAgeMin($randomAgeMin)
            ->setAgeMax($randomAgeMin + 20)
            ->addDepartment($user->getDepartment())
            ->addShape($user->getShape())
            ->addSkin($user->getSkin())
            ->addEye($user->getEyes());

        return $searchPreferences;
    }

    /**
     * @param Hobby[] $hobbies
     * @param User    $user
     *
     * @return UserHobby[]
     */
    private function getRandomUserHobbies($hobbies, $user)
    {
        $rateMin    = -5;
        $rateMax    = 5;
        $randomRate = mt_rand($rateMin, $rateMax);

        $randomHobbies = [];
        for ($i = 0; $i < 20; ++$i) {
            $randomHobbieKey = array_rand($hobbies);
            $hobby           = $hobbies[$randomHobbieKey];
            $userHobby       = new UserHobby();
            $userHobby->setHobby($hobby)
                ->setPriority($randomRate)
                ->setAsPreference((bool) mt_rand(0, 1))
                ->setUser($user);
            $randomHobbies[] = $userHobby;
        }

        return $randomHobbies;
    }
}
