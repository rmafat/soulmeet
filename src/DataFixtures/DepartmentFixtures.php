<?php

namespace App\DataFixtures;

use App\Entity\Department;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class DepartmentFixtures
 */
class DepartmentFixtures extends Fixture
{
    /** @var HttpClientInterface */
    private $client;

    /**
     * DepartmentFixtures constructor.
     *
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://geo.api.gouv.fr/departements'
            );
            if (200 === $response->getStatusCode()) {
                $wsDepartments = $response->toArray();
                foreach ($wsDepartments as $wsDepartment) {
                    if (array_key_exists('nom', $wsDepartment) && array_key_exists('codeRegion', $wsDepartment) && array_key_exists('code', $wsDepartment)) {
                        $department = new Department();
                        $department->setName($wsDepartment['nom'])
                            ->setNumber((int) $wsDepartment['code'])
                            ->setRegion((int) $wsDepartment['codeRegion']);
                        $manager->persist($department);
                    }
                }
                $manager->flush();
            } else {
                throw new Exception('Unable to get departments from https://geo.api.gouv.fr/departements. Status code :'.$response->getStatusCode());
            }
        } catch (TransportExceptionInterface | ClientExceptionInterface | DecodingExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
            throw $e;
        }
    }
}
