<?php

namespace App\Entity;

use App\Repository\HobbyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HobbyRepository::class)
 */
class Hobby
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=Picture::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $icon;

    /**
     * @ORM\OneToMany(targetEntity=UserHobby::class, mappedBy="hobby", orphanRemoval=true)
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIcon(): ?Picture
    {
        return $this->icon;
    }

    public function setIcon(Picture $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|UserHobby[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(UserHobby $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setHobby($this);
        }

        return $this;
    }

    public function removeUser(UserHobby $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getHobby() === $this) {
                $user->setHobby(null);
            }
        }

        return $this;
    }
}
