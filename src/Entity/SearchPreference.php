<?php

namespace App\Entity;

use App\Repository\SearchPreferenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SearchPreferenceRepository::class)
 */
class SearchPreference
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $gender;

    /**
     * @ORM\ManyToMany(targetEntity=Department::class)
     */
    private $department;

    /**
     * @ORM\ManyToMany(targetEntity=Skin::class)
     */
    private $skin;

    /**
     * @ORM\ManyToMany(targetEntity=Eyes::class)
     */
    private $eyes;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ageMin;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ageMax;

    /**
     * @ORM\ManyToMany(targetEntity=Shape::class)
     */
    private $shape;

    public function __construct()
    {
        $this->department = new ArrayCollection();
        $this->skin       = new ArrayCollection();
        $this->eyes       = new ArrayCollection();
        $this->shape      = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Collection|Department[]
     */
    public function getDepartment(): Collection
    {
        return $this->department;
    }

    public function addDepartment(Department $department): self
    {
        if (!$this->department->contains($department)) {
            $this->department[] = $department;
        }

        return $this;
    }

    public function removeDepartment(Department $department): self
    {
        $this->department->removeElement($department);

        return $this;
    }

    /**
     * @return Collection|Skin[]
     */
    public function getSkin(): Collection
    {
        return $this->skin;
    }

    public function addSkin(Skin $skin): self
    {
        if (!$this->skin->contains($skin)) {
            $this->skin[] = $skin;
        }

        return $this;
    }

    public function removeSkin(Skin $skin): self
    {
        $this->skin->removeElement($skin);

        return $this;
    }

    /**
     * @return Collection|Eyes[]
     */
    public function getEyes(): Collection
    {
        return $this->eyes;
    }

    public function addEye(Eyes $eye): self
    {
        if (!$this->eyes->contains($eye)) {
            $this->eyes[] = $eye;
        }

        return $this;
    }

    public function removeEye(Eyes $eye): self
    {
        $this->eyes->removeElement($eye);

        return $this;
    }

    public function getAgeMin(): ?int
    {
        return $this->ageMin;
    }

    public function setAgeMin(int $ageMin): self
    {
        $this->ageMin = $ageMin;

        return $this;
    }

    public function getAgeMax(): ?int
    {
        return $this->ageMax;
    }

    public function setAgeMax(int $ageMax): self
    {
        $this->ageMax = $ageMax;

        return $this;
    }

    /**
     * @return Collection|Shape[]
     */
    public function getShape(): Collection
    {
        return $this->shape;
    }

    public function addShape(Shape $shape): self
    {
        if (!$this->shape->contains($shape)) {
            $this->shape[] = $shape;
        }

        return $this;
    }

    public function removeShape(Shape $shape): self
    {
        $this->shape->removeElement($shape);

        return $this;
    }
}
