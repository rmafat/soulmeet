<?php

namespace App\Repository;

use App\Entity\SearchPreference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SearchPreference|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchPreference|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchPreference[]    findAll()
 * @method SearchPreference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchPreferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SearchPreference::class);
    }

    // /**
    //  * @return SearchPreference[] Returns an array of SearchPreference objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SearchPreference
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
