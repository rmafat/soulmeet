<?php
return PhpCsFixer\Config::create()
    ->setRules(array(
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'array_syntax' => array('syntax' => 'short'),
        'no_unreachable_default_argument_value' => false,
        'no_superfluous_phpdoc_tags' => false,
        'braces' => array('allow_single_line_closure' => true),
        'heredoc_to_nowdoc' => false,
        'phpdoc_annotation_without_dot' => false,
        'phpdoc_summary' => false,
        'native_function_invocation' => false,
        'binary_operator_spaces' =>
            [
                'operators' => [
                    '=>' => 'align_single_space_minimal',
                    '=' => 'align_single_space_minimal',
                ]
            ]
    ,
    ))
    ->setRiskyAllowed(true)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__.'/src')
            ->exclude(array(
                'vendor',
                'web',
            ))
    );

